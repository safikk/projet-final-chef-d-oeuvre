package co.simplon.promo18.projetfinalback.entities;

import java.time.LocalDate;
import java.util.List;

public class Comment {
    private Integer id;
    private String comment;
    private LocalDate date;
    private User user;
    private Article article;
    public Comment() {
    }

    public Comment(Integer id, String comment, LocalDate date) {
        this.id = id;
        this.comment = comment;
        this.date = date;
    }

    public Comment(String comment, LocalDate date, User user, Article article) {
        this.comment = comment;
        this.date = date;
        this.user = user;
        this.article = article;
    }


    public Comment(Integer id, String comment, LocalDate date, User user, Article article) {
        this.id = id;
        this.comment = comment;
        this.date = date;
        this.user = user;
        this.article = article;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;

    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}