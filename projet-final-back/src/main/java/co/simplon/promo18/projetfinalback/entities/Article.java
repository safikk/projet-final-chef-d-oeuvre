package co.simplon.promo18.projetfinalback.entities;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class Article {
    private Integer id;
    private String title;
    private String description;
    private String image;
    private LocalDate date;
    private List<Comment> comments = new ArrayList<>();
    private int categoryId;
    
    public Article(String title, String description, String image, LocalDate date, Integer categoryId) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
        this.categoryId = categoryId;
    }
    public Article() {
    }
    public Article(String title, String description, String image, LocalDate date) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
    }
    public Article(Integer id, String title, String description, String image, LocalDate date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate localDate) {
        this.date = localDate;
    }
    public void setComment(List<Comment> listComment) {
    }
    public List<Comment> getComments() {
        return comments;
    }
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}