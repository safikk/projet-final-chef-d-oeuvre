package co.simplon.promo18.projetfinalback.controller;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinalback.entities.Article;
import co.simplon.promo18.projetfinalback.repository.ArticleRepository;
import co.simplon.promo18.projetfinalback.repository.CommentRepository;

@RestController
public class ArticleController {
    @Autowired
    ArticleRepository repo;
    @Autowired
    private CommentRepository commentRepo;

    @GetMapping("/api/article")
    public List<Article> all(){
        return repo.findAll();
    }


    // @PostMapping("/api/article")
    // public Article add (@RequestBody Article article){
    //     if (article.getDate() == null ){
    //         article.setDate(LocalDate.now());
        
            
    //     }
    //     repo.save(article);
    //     return article;
    // }
    @DeleteMapping("/api/article/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable int id){
        if (!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    
    @PutMapping("/api/article/{id}")
    public Article update(@PathVariable int id,
     @RequestBody Article article) {
         if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }
        if(!repo.update(article)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }

    @GetMapping("/api/article/{id}") //
    public Article one(@PathVariable int id) {
        Article article = repo.findById(id);
        if (article == null) { 
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        article.setComments(commentRepo.findByarticleId(id));

        return article;
    }

    @GetMapping("/api/article/category/{id}")
    public List<Article> finByCategory(@PathVariable int id) {
        return repo.findByIdCategory(id);
    }




    @PostMapping("/api/article")
    public Article add(@ModelAttribute @Valid Article post, 
                    @ModelAttribute MultipartFile upload)
                 {
        // post.setAuthor(user);
        post.setDate(LocalDate.now());
        System.out.println(post.getCategoryId());
        post.setId(null);

        //On créer un nom unique pour le fichier pour éviter les collisions de noms (genre 2 personnes qui upload un fichier chat.jpg)
        //et on fait en sorte de récupérer l'extension du fichier original pour la concaténer à ce nom
        String filename = UUID.randomUUID() + "."+ FilenameUtils.getExtension(upload.getOriginalFilename());
        // Pour changer le nom de la photo
        try {
            //On transfère le fichier dans le fichier de la requête dans le dossier d'upload, avec son nouveau nom
            upload.transferTo(new File(getUploadFolder(), filename));
            //On stock le nom du fichier sur la base de donnée pour pouvoir savoir quel fichier est lié à quel Post
            post.setImage(filename);
            repo.save(post);
        } catch (IllegalStateException | IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "File upload failed");
        }
        return post;
    }

    /**
     * Méthode qui va récupérer le dossier upload (qui sera le dossier static/uploads dans le projet)
     * et le créer si jamais ce dossier n'existe pas.
     * Pour récupérer le dossier cible, on se sert du classLoader.getResource(".") qui renvoie le
     * dossier où se trouve le code exécuté du serveur.
     * Dans l'idéal, dans un contexte de production, il serait préférable d'avoir un dossier
     * d'upload séparé du projet, mais dans un contexte de développement, c'est plus portatif
     * de faire comme ça.
     */
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/image"));
        if(!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
            
    }


}
