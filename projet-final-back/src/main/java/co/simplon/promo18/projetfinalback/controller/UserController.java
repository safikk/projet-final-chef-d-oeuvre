package co.simplon.promo18.projetfinalback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.projetfinalback.entities.User;
import co.simplon.promo18.projetfinalback.repository.UserRepository;

@RestController
public class UserController {
    
    @Autowired
    UserRepository repo;

    @GetMapping("/api/user")
    public List<User> all() {
        return repo.findAll();
    }
    @GetMapping("api/user/{id}")
    public User one (@PathVariable int id){
        User user = repo.findById(id);
        return user;
    }

    @PostMapping("/api/user")
    public User add (@RequestBody User user){
        repo.save(user);
        return user;
    }

    @DeleteMapping("/api/user/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete (@PathVariable int id){
        if (!repo.deleteById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/api/user/{id}")
    public User update(@PathVariable int id,
     @RequestBody User user) {
         if (id != user.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
         }
        if(!repo.update(user)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(user.getId());
    }
}

