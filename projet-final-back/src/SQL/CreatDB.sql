-- Active: 1652189003347@@127.0.0.1@3306@Projet_Final

DROP DATABASE IF EXISTS Projet_Final;

CREATE DATABASE Projet_Final;

USE Projet_Final;

CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(64),
    last_name VARCHAR(64),
    email VARCHAR(64),
    password VARCHAR(64),
    role VARCHAR(64)
);

CREATE TABLE category (
    id INT AUTO_INCREMENT PRIMARY KEY,
    label VARCHAR(64)
);

CREATE TABLE article (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(265),
    description TEXT,
    image VARCHAR(128),
    date DATE,
    user_id INTEGER,
    category_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE comment(
    id INT AUTO_INCREMENT PRIMARY KEY,
    comment VARCHAR (500),
    date DATE,
    article_id INTEGER,
    user_id INTEGER,
    FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);

INSERT INTO category (label) VALUES
("Europe"),
("Asie"),
("Afrique"),
("Amérique"),
("Australie");

INSERT INTO article
(title,description,image,date,category_id) VALUES
("Mon voyage en Grèce",
"La Grèce, aussi appelée Hellas ou francisé en Hellade, en forme longue 
la République hellénique  en grec ancien, est un pays d’Europe du Sud 
et des Balkans. D'une superficie de 131 957 km2 pour un peu moins de 
onze millions d'habitants",
"Grece1.jpg",
"2022-06-15"
,1),

("Mon voyage au Japon",
"Le Japon est un pays insulaire de l'Asie de l'Est, situé entre l'océan Pacifique 
et la mer du Japon, à l'est de la Chine, de la Corée du Sud, de la Corée du Nord 
et de la Russie, et au nord de Taïwan. Étymologiquement, les kanjis (caractères 
chinois) qui composent le nom du Japon signifient pays d'origine du Soleil; c'est 
ainsi que le Japon est désigné comme le pays du soleil levant.
Le Japon forme, depuis 1945, un archipel de 6 852 îles (de plus de 100 m2), dont 
les quatre plus grandes sont Hokkaidō, Honshū, Shikoku et Kyūshū, représentant à 
elles seules 95 % de la superficie terrestre du pays. L'archipel s'étend sur plus 
de trois mille kilomètres.",
"imageJapon.jpg",
"2020-05-10",
2),

("Mon voyage en Egypte", 
"L'Égypte est un pays transcontinental se trouvant en Afrique du Nord-Est et, pour 
la péninsule du Sinaï, en Asie de l'Ouest. Située sur la côte sud de la Méditerranée 
orientale, le bassin Levantin, le pays a des frontières terrestres avec la Libye à 
l'ouest, le Soudan au sud, la Mer rouge à l'est, et Israël et la bande de Gaza de la 
Palestine au nord-est. La capitale, et la ville la plus peuplée du pays, est Le Caire. 
Avec plus de 102 millions d'habitants en 2020, l'Égypte est le troisième pays le plus 
peuplé d'Afrique derrière le Nigeria et l'Éthiopie. La surface du pays est largement 
recouverte par le Sahara et la population est fortement concentrée aux côtes du Nil.
L'Égypte a l'une des plus longues histoires de tous les pays. Berceau de la civilisation, 
l'Égypte antique a vu parmi les premiers développements de l'écriture, l'agriculture, 
le gouvernement centralisé et la religion organisée au cours de plus de trois millénaires 
avant l'Ère commune. Des monuments emblématiques tels le Sphinx de Gizeh, les pyramides 
de Gizeh et la vallée des Rois reflètent cet héritage et demeurent des sources importantes 
d'intérêt scientifique et populaire.",
"imageEgypte.jpg",
"2018-09-29",
3);

INSERT INTO user
(first_name, last_name,email,password,role) VALUES
("user_firstname","user_lastname","user@test.com","1234","ROLE_USER"),
("admin_name","admin_firstname","admin@test.com","1234","ROLE_ADMIN"),
("Nicos","Petros","nicos.petros@gmail.com","1234","ROLE_USER"),
("Oshimo","Fujiko","oshimo.fujiko@gmail.com","1234","ROLE_USER");


INSERT INTO comment (comment,date) VALUES
("Pays fabuleux où nous avons eu l'occasion d'y aller avec mon conjoint, nous y retournerons très vite","2022-07-20"),
("Extraordinaire, nous avons eu la chance d'avoir du soleil, c'était très agréable","2022-08-09");

select * from comment inner join article on article.id=comment.article_id WHERE article.id=1;

select * from article WHERE category_id=1;
