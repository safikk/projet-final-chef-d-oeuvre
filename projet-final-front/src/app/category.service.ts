import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from './entities';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Category[]>(environment.apiUrl + '/api/category');
  }

  // getAll() {
  //   return this.http.get<Category[]>('http://localhost:8080/api/category');
  // }

  getById(id: number) {
    return this.http.get<Category>('http://localhost:8080/api/category/' + id)
  }

  getByCategory(id: number) {
    return this.http.get<Category>('http://localhost:8080/api/category/' + id)
  }
}


