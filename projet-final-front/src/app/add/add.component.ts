import { Component, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private articleService:ArticleService, private categoryService:CategoryService, private router:Router) { }

  categories: Category[] = [];
  // categoryId?: number;

  article:Article = {
    id: 0,
    title: '',
    image: '',
    content: '',
    description: '',
    date: '',
    categoryId: 1,
  };

  idCategory = 0
  selection: EventEmitter<number> = new EventEmitter()

  upload?:File;

  displayAddArticle:boolean = false;


  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => this.categories = data);
  }

  showAddArticleForm() {
    if (!this.displayAddArticle) {
      this.displayAddArticle = true;
    } else if (this.displayAddArticle) {
      this.displayAddArticle = false;

    }

  }

  addArticle() {
    console.log(this.article);
    this.articleService.add(this.article, this.upload!).subscribe((data) => {
      this.router.navigate(['/article',data.id ]);
    }); 
 //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }


  changeFile(event: any) {
    if (event.target.files.length > 0) {
      this.upload = event.target.files[0]; //On récupère le fichier mis dans l'input et on le stock dans le component
    }
  }

  select() {
    this.selection.emit(this.idCategory)
  }

  // addArticle() {
  //   let now = new Date();
  //   this.article!.date = now.toISOString();
  //   if (this.categoryId == undefined) {
  //     alert("Merci de saisir une catégorie !");
  //   }
  //   else if ((this.article.title.length == 0) || (this.article.content.length == 0) || (this.article.description.length == 0)) {
  //     alert("Il semble qu'un champ au moins soit vide")
  //   }
  //   else {
  //     this.articleService.addArticle(this.article, this.categoryId).subscribe();
  //     //redirect to home
  //     this.router.navigateByUrl('');
  //   }


  // }
  // getCategories() {
  //   this.categoryService.getAllCategoriesWithArticles().subscribe((data: Category[]) => {
  //     this.categories = data
  //     console.log(this.categories)
  //   });
  // }


}

