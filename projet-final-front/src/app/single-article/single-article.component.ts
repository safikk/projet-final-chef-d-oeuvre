import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { ArticleService } from '../article.service';
import { Article, Comment } from '../entities';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {


  
  constructor(private articleService:ArticleService, private route:ActivatedRoute, private router:Router) { }
  article?:Article;
  comment?:Comment;
  
  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.articleService.getById(params['id']))
    )
    .subscribe(data => this.article = data);
  }

  delete(id:number) {
    console.log(id);
    
    this.articleService.delete(id).subscribe();
    this.router.navigate(['/']);

  }

  // removeFromList(id: number) {
  //   throw new Error('Method not implemented.');
  // }

  

}
