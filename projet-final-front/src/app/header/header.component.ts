import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(config: NgbCarouselConfig) { 
    config.interval = 2000;
    config.keyboard = true;
    config.pauseOnHover = true;
    
  }

  images = [
    {title: 'First Slide', short: 'First Slide Short', src: "/assets/image/imageMaps5.jpg"},
    {title: '', short: '', src: "/assets/image/home-image1.jpg"},
    {title: '', short: '', src: "/assets/image/imageMpas6.jpg"}
  ];

  ngOnInit(): void {
  }

}
