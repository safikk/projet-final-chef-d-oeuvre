import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../article.service';
import { CategoryService } from '../category.service';
import { Article, Category } from '../entities';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selected?:Article;
  articles:Article[] = [];
  categories:Category[] = [];
  selCategory:number = 0;
  displayEditForm:boolean = false;
  // str!: '';
  modifiedArticle:Article = {
    title: "", description: "", image: "", date: "",
    content: undefined,
    // article: undefined
  };

  
  // injection
  constructor(private articleService:ArticleService, private categoryService:CategoryService) { }

  // ce qui va s'executer à l'initialisation de mon composant
  ngOnInit(): void {
    
    this.categoryService.getAll().subscribe(data => this.categories = data)
   
    this.articleService.getAll().subscribe(data => this.articles = data);
  }

  fetchAll() {
    this.articleService.getAll().subscribe(data => this.articles = data);
  }

  
  filter() {
    if (this.selCategory>0) {
      this.articleService.getByIdCategory(this.selCategory).subscribe(data => this.articles = data);
    } else {
      this.fetchAll()
    }
  }

  fetchOne() {
    this.articleService.getById(1).subscribe(data => {
      this.selected = data
      console.log(this.selected)
    });
  }

  // delete(id:number) {
  //   this.articleService.delete(id).subscribe();
  //   this.removeFromList(id);
  // }

  // removeFromList(id: number) {
  //   this.article.forEach((article, article_index) => {
  //     if(article.id == id) {
  //       this.article.splice(article_index, 1);
  //     }
  //   });
  // }

  // fetchAll(){
  //   this.articleService.getAll().subscribe(data => this.article = data);
  // }

  // showEditForm(article:Article) {
  //   this.displayEditForm = true;
    
  //   this.modifiedArticle = Object.assign({},article);
  // }

  // update(article:Article) {
  //   // Envoyer au serveur
  //   this.articleService.put(article).subscribe();
  //   // Mettre à jour la liste en HTML
  //   this.updateArticleInList(article);
  //   // Fermer le formulaire
  //   this.displayEditForm = false;
  // }

  // updateArticleInList(article:Article) {
  //   this.article.forEach((article_list) => {
  //     if(article_list.id == article.id) {
  //       article_list.title = article.title;
  //       article_list.date = article.date;
  //       article_list.content = article.content;
  //     }
  //   });
  // }



}
