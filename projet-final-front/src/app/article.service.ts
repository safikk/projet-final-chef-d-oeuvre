import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Article } from './entities';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Article[]>(environment.apiUrl+'/api/article');
  }

  getById(id: number) {
    return this.http.get<Article>(environment.apiUrl+'/api/article/' + id)
  }

  // add(article:Article){
  //   return this.http.post<Article>(environment.apiUrl+'/api/article', article);
  // }

  delete(id: number) {
    return this.http.delete(environment.apiUrl+'/api/article/' + id);
  }

  put(article: Article) {
    return this.http.put<Article>(environment.apiUrl+'/api/article/' + article.id, article);
  }



  add(article: Article, upload: File) {
    const form = new FormData();
    form.append('title', article.title);
    form.append('description', article.description);
    form.append('upload', upload);

    if (article.categoryId) {
      form.append('categoryId', article.categoryId.toString());
    }

console.log(form)
    return this.http.post<Article>(environment.apiUrl+'/api/article', form);
  }
  // formdata est utilisé pour envoyer des informations


  // getByIdCategory(id:number) {
  //   return this.http.get<Article[]>('http://localhost:8080/api/article/category/'+id);
  // }

  getByIdCategory(id: number) {
    return this.http.get<Article[]>(environment.apiUrl+'/api/article/category/' + id);

  }
}
