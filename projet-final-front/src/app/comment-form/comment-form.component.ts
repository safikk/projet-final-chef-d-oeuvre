import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CommentService } from '../comment.service';
import { Article, Comment } from '../entities';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  @Input()
  article?: Article;

  inputComment = "";

  constructor(private commentService: CommentService, private router: Router) { }

  ngOnInit(): void {
  }


  addComment() {

    if (this.article) {
      const comment: Comment = {
        id: 0,
        comment: this.inputComment,
        date: new Date().toISOString().slice(0, 10)
      };
      this.commentService.add(comment, this.article.id!).subscribe(data => {
        if (!this.article!.comments) {
          this.article!.comments = []
        }
        this.article!.comments?.push(comment)

        console.log(this.article?.comments)
        this.inputComment = ""
      });
    }
  }

}

