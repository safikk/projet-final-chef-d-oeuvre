export interface Article {
    content: any;
    id?: number;
    title: string;
    description: string;
    image: string;
    date?: string;
    categoryId?: number;
    comments?: Comment[];
    categories?: Category[];
}

export interface Comment {
    id?: number;
    comment: string;
    date: string;
    comments?: Comment[];
}

export interface Category {
    id: number;
    label: string;
}

export interface User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    role: string;
}
